package nl.naturalis.log.appenders.logback;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import nl.naturalis.check.Check;
import nl.naturalis.log.appenders.InvalidLoggerException;
import nl.naturalis.log.appenders.LiveLogStream;
import static java.util.stream.Collectors.toUnmodifiableMap;
import static nl.naturalis.check.CommonChecks.notNull;
import static org.slf4j.Logger.ROOT_LOGGER_NAME;
import static nl.naturalis.common.ArrayMethods.EMPTY_STRING_ARRAY;
import static nl.naturalis.common.ArrayMethods.pack;
import static nl.naturalis.common.ObjectMethods.ifNull;
import static nl.naturalis.log.appenders.InvalidLoggerException.missingLogLevel;

public class LogbackLiveLogStream extends LiveLogStream {

  public static class Builder extends LiveLogStream.Builder {
    @Override
    public LogbackLiveLogStream build() {
      return new LogbackLiveLogStream(out, loggers, additiveLoggers, pattern, level);
    }
  }

  private final Map<Logger, Level> loggers;
  private final Map<Logger, Boolean> additive;
  private final PatternLayoutEncoder encoder;
  private final OutputStreamAppender<ILoggingEvent> appender;

  private LogbackLiveLogStream(
      OutputStream out,
      String[] loggerNames,
      String[] additiveLoggers,
      String pattern,
      String level) {
    super(out);
    LoggerContext ctx = (LoggerContext) LoggerFactory.getILoggerFactory();
    loggers =
        Arrays.stream(ifNull(loggerNames, pack(ROOT_LOGGER_NAME)))
            .peek(this::checkLoggerName)
            .map(ctx::getLogger)
            .map(ch.qos.logback.classic.Logger.class::cast)
            .peek(logger -> checkLevel(logger))
            .collect(toUnmodifiableMap(Function.identity(), Logger::getLevel));
    if (level != null) {
      loggers.keySet().forEach(logger -> logger.setLevel(Level.toLevel(level)));
    }
    additive =
        Arrays.stream(ifNull(additiveLoggers, EMPTY_STRING_ARRAY))
            .peek(this::checkLoggerName)
            .map(ctx::getLogger)
            .map(ch.qos.logback.classic.Logger.class::cast)
            .collect(toUnmodifiableMap(Function.identity(), Logger::isAdditive));
    additive.keySet().forEach(logger -> logger.setAdditive(true));
    encoder = new PatternLayoutEncoder();
    encoder.setPattern(ifNull(pattern, DEFAULT_PATTERN));
    encoder.setContext(ctx);
    encoder.start();
    appender = new OutputStreamAppender<>();
    appender.setContext(ctx);
    appender.setEncoder(encoder);
    appender.setImmediateFlush(true);
    appender.setOutputStream(out);
    loggers.keySet().forEach(logger -> logger.addAppender(appender));
    appender.start();
  }

  private static void checkLevel(Logger logger) {
    Check.on(s -> missingLogLevel(logger.getName()), logger.getLevel()).is(notNull());
  }

  @Override
  public void close() throws IOException {
    additive.forEach(Logger::setAdditive);
    appender.stop();
    encoder.stop();
    loggers.forEach(Logger::setLevel);
    loggers.keySet().forEach(logger -> logger.detachAppender(appender));
    out.flush();
  }

  private void checkLoggerName(String name) {
    ((LoggerContext) LoggerFactory.getILoggerFactory())
        .getLoggerList().stream()
            .map(Logger::getName)
            .filter(s -> name.equals(ROOT_LOGGER_NAME) || s.equals(name))
            .findFirst()
            .orElseThrow(() -> InvalidLoggerException.noSuchLogger(name));
  }
}
