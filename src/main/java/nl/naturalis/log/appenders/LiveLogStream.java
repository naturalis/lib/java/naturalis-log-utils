package nl.naturalis.log.appenders;

import java.io.OutputStream;
import nl.naturalis.check.Check;
import nl.naturalis.log.LogImpl;
import nl.naturalis.log.appenders.log4j.Log4JLiveLogStream;
import nl.naturalis.log.appenders.logback.LogbackLiveLogStream;
import static nl.naturalis.check.CommonChecks.*;

/**
 * Utility class that temporarily, within the bounds of a try-with-resources block, sets up an
 * <code>Appender</code> that outputs to an arbitrary <code>Outstream</code>. This can be used to
 * "redirect" log messages to, for example, the HTTP response output stream. (In fact the log
 * messages still also get written to any other configured appender.) This class isn't really an
 * <code>Appender</code>, but rather one that sets one up and tears it down again. Example:
 *
 * <pre>
 * ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
 * try(LiveLogStream lls = LiveLogStream.create(LogImpl.LOGBACK, out "DEBUG") {
 *  // code ...
 *  LOG.debug(" ... ")
 *  // more code
 * }
 * return Response.status(200).entity(out.toString()).build();
 * </pre>
 *
 * @author Ayco Holleman
 */
public abstract class LiveLogStream implements AutoCloseable {

  /** The default pattern used by the temporary appender: <code>%d{HH:mm:ss.SSS}  %msg%n</code>. */
  public static final String DEFAULT_PATTERN = "%d{HH:mm:ss.SSS}  %msg%n";

  /**
   * Creates a <code>LiveLogStream</code> streaming log messages to the provided <code>OutputStream
   * </code>.
   *
   * @param impl
   * @param out
   * @return
   */
  public static LiveLogStream create(LogImpl impl, OutputStream out) {
    return builderFor(impl).withOutputStream(out).build();
  }

  /**
   * Creates a <code>LiveLogStream</code> streaming log messages to the provided <code>OutputStream
   * </code> while temporarily overriding the log level of all loggers with the provided log level.
   *
   * @param impl
   * @param out
   * @param logLevel
   * @return
   */
  public static LiveLogStream create(LogImpl impl, OutputStream out, String logLevel) {
    return builderFor(impl).withOutputStream(out).withLogLevel(logLevel).build();
  }

  /**
   * Creates a <code>LiveLogStream</code> streaming log messages from a limited set of loggers to
   * the provided <code>OutputStream</code> while temporarily overriding the log level of all
   * loggers with the provided log level.You can use an asterisk to specify a logger and all of its
   * descendants. For example: <code>nl.naturalis.*</code>.
   *
   * @param impl
   * @param out
   * @param logLevel
   * @param loggers
   * @return
   */
  public static LiveLogStream create(
      LogImpl impl, OutputStream out, String logLevel, String... loggers) {
    return builderFor(impl)
        .withOutputStream(out)
        .withLoggers(loggers)
        .withLogLevel(logLevel)
        .build();
  }

  /**
   * A <code>Builder</code> class for <code>LiveLogStream</code> instances.
   *
   * @author Ayco Holleman
   */
  public abstract static class Builder {
    protected OutputStream out;
    protected String[] loggers;
    protected String[] additiveLoggers;
    protected String level;
    protected String pattern;

    protected Builder() {}

    /**
     * The output stream to which to write the log messages. Required.
     *
     * @param out
     * @return
     */
    public Builder withOutputStream(OutputStream out) {
      this.out = out;
      return this;
    }

    /**
     * The logger(s) to attach the appender to. For example: <code>nl.naturalis</code>. You can use
     * an asterisk to specify a logger and all of its descendants. For example: <code>nl.naturalis.*
     * </code>. If you don't specify any logger, <i>all</i> loggers, including the root logger, will
     * write to the output stream.
     *
     * @param loggers
     * @return
     */
    public Builder withLoggers(String... loggers) {
      this.loggers = Check.that(loggers, "loggers").is(deepNotNull()).ok();
      return this;
    }

    /**
     * Temporarily sets the <code>additivity</code> property of the specified loggers to <code>true
     * </code>. Note that if one or more of these loggers were initially non-additive, you will
     * likely get duplicate lines in the regular log file and/or the console log. This will last
     * while the <code>LiveLogStream</code> is active. The original additivity settings of the
     * loggers will be restored once the <code>LiveLogStream</code> closes.
     *
     * @param loggers
     * @return
     */
    public Builder withAdditiveLoggers(String... loggers) {
      this.additiveLoggers = Check.that(loggers, "loggers").is(deepNotNull()).ok();
      return this;
    }

    /**
     * Temporarily sets the log level of the loggers specified through {@link
     * #withLoggers(String...) withLoggers} to the specified log level. Log levels will be reset to
     * their initial value once the <code>LiveLogStream</code> closes. If no log level is specified,
     * the loggers will keep logging at their configured levels.
     *
     * @param level
     * @return
     */
    public Builder withLogLevel(String level) {
      this.level = Check.notNull(level, "level").ok();
      return this;
    }

    /**
     * The message pattern to be used by the appender. If no pattern is specified, the {@link
     * #DEFAULT_PATTERN} is used.
     *
     * @param pattern
     * @return
     */
    public Builder withPattern(String pattern) {
      this.pattern = Check.notNull(pattern, "pattern").ok();
      return this;
    }

    /**
     * Returns a <code>LiveLogStream</code> configured using this <code>Builder</code> instance.
     *
     * @return
     */
    public abstract LiveLogStream build();
  }

  /**
   * Returns a <code>Builder</code> capable of producing <code>LiveLogStream</code> instances for a
   * specific logging implementation (e&#46;g&#46; log4j or logback).
   *
   * @param logImpl
   * @return
   */
  public static Builder builderFor(LogImpl logImpl) {
    if (logImpl == LogImpl.LOGBACK) {
      return new LogbackLiveLogStream.Builder();
    }
    return new Log4JLiveLogStream.Builder();
  }

  protected final OutputStream out;

  protected LiveLogStream(OutputStream out) {
    this.out = Check.notNull(out, "out").ok();
  }

  /**
   * Flushes the <code>OutputStream</code>, stops and detaches the <code>Appender</code> and
   * restores the initial log level and additivity of the loggers. Note however that <i>the</i>
   * <code>OutputStream</code> <i>itself is not closed by this method</i>.
   */
  @Override
  public void close() throws Exception {
    // no-op just here for the javadocs
  }

  /**
   * Returns the <code>OutputStream</code> to which the data is written.
   *
   * @return
   */
  public OutputStream getOutputStream() {
    return out;
  }
}
