package nl.naturalis.log.appenders.log4j;

import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.OutputStreamAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;
import nl.naturalis.check.Check;
import nl.naturalis.log.appenders.LiveLogStream;
import static java.util.stream.Collectors.toMap;
import static nl.naturalis.common.ArrayMethods.EMPTY_STRING_ARRAY;
import static nl.naturalis.common.ObjectMethods.ifNotNull;
import static nl.naturalis.common.ObjectMethods.ifNull;
import static nl.naturalis.common.StringMethods.substrBefore;

public class Log4JLiveLogStream extends LiveLogStream {

  private static final org.apache.logging.log4j.Logger myLogger =
      LogManager.getLogger(Log4JLiveLogStream.class);

  public static class Builder extends LiveLogStream.Builder {
    @Override
    public LiveLogStream build() {
      return new Log4JLiveLogStream(out, loggers, additiveLoggers, pattern, level);
    }
  }

  private final Appender appender;
  private final String[] loggerNames;
  // Stores the original level of the involved loggers
  private final Map<LoggerConfig, Level> levels = new HashMap<>();
  // Stores the original additivity of the involved loggers
  private final Map<LoggerConfig, Boolean> additivities;
  private final LoggerContext context;
  private final Configuration config;

  private Log4JLiveLogStream(
      OutputStream out,
      String[] loggerNames,
      String[] additiveLoggers,
      String pattern,
      String level) {
    super(out);
    Check.that(level, "level").is(this::isValidLevel);
    myLogger.debug("Starting LiveLogStream");
    this.loggerNames = loggerNames;
    this.context = LoggerContext.getContext(false);
    this.config = context.getConfiguration();
    this.appender = createAppender(pattern);
    attachToLoggers(level);
    additivities =
        Arrays.stream(ifNull(additiveLoggers, EMPTY_STRING_ARRAY))
            .map(config::getLoggerConfig)
            .collect(toMap(Function.identity(), LoggerConfig::isAdditive));
    additivities.keySet().forEach(logger -> logger.setAdditive(true));
    context.updateLoggers(config);
  }

  @Override
  public void close() throws Exception {
    appender.stop();
    out.flush();
    config.getAppenders().remove(appender.getName());
    detachFromLoggers();
    // Restore log levels
    levels.forEach(LoggerConfig::setLevel);
    // Restore additivity settings
    additivities.forEach(LoggerConfig::setAdditive);
    context.updateLoggers(config);
  }

  private Appender createAppender(String pattern) {
    PatternLayout layout =
        PatternLayout.newBuilder().withPattern(ifNull(pattern, DEFAULT_PATTERN)).build();
    String name = "LIVE_LOG_STREAM_" + System.identityHashCode(this);
    Appender appender = OutputStreamAppender.createAppender(layout, null, out, name, false, true);
    new OutputStreamAppender.Builder<>().setConfiguration(config);
    appender.start();
    config.addAppender(appender);
    return appender;
  }

  private void attachToLoggers(String logLevel) {
    Level level = ifNotNull(logLevel, Level::toLevel);
    if (loggerNames == null) {
      config.getLoggers().values().stream()
          .filter(logger -> logger != config.getRootLogger())
          .forEach(logger -> attachToLogger(logger, level));
    } else {
      for (String name : loggerNames) {
        if (name.endsWith(".*")) {
          config.getLoggers().values().stream()
              .filter(logger -> logger.getName().startsWith(substrBefore(name, ".*", 1)))
              .forEach(logger -> attachToLogger(logger, level));
        } else {
          config.getLoggers().values().stream()
              .filter(logger -> logger.getName().equals(name))
              .findFirst()
              .ifPresent(logger -> attachToLogger(logger, level));
        }
      }
    }
  }

  private void attachToLogger(LoggerConfig loggerConfig, Level level) {
    loggerConfig.addAppender(appender, ifNull(level, loggerConfig::getLevel), null);
    if (level != null) {
      levels.put(loggerConfig, loggerConfig.getLevel());
      loggerConfig.setLevel(level);
    }
  }

  private void detachFromLoggers() {
    if (loggerNames == null) {
      config.getLoggers().values().forEach(l -> l.removeAppender(appender.getName()));
      config.getRootLogger().removeAppender(appender.getName());
    } else {
      for (String name : loggerNames) {
        if (name.endsWith(".*")) {
          config.getLoggers().values().stream()
              .filter(l -> l.getName().startsWith(substrBefore(name, ".*", 1)))
              .forEach(l -> l.removeAppender(appender.getName()));
        } else {
          config.getLoggers().values().stream()
              .filter(l -> l.getName().equals(name))
              .findFirst()
              .ifPresent(l -> l.removeAppender(appender.getName()));
        }
      }
    }
  }

  private boolean isValidLevel(String level) {
    if (level != null) {
      try {
        Level.valueOf(level);
      } catch (IllegalArgumentException e) {
        return false;
      }
    }
    return true;
  }
}
