package nl.naturalis.log.appenders;

public class InvalidLoggerException extends IllegalArgumentException {

  public static InvalidLoggerException noSuchLogger(String name) {
    return new InvalidLoggerException(String.format("No such logger: \"%s\"", name));
  }

  public static InvalidLoggerException missingLogLevel(String name) {
    return new InvalidLoggerException(
        String.format("Cannot determine log level for logger \"%s\"", name));
  }

  public InvalidLoggerException(String msg) {
    super(msg);
  }
}
