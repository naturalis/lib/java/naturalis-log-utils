package nl.naturalis.log.im;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/** The payload to be sent to the messenging app. */
@JsonPropertyOrder({"text", "channel", "username", "icon_url"})
public class Payload {

  private String text;
  private String channel;
  private String userName;
  private String iconUrl =
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/OpenMoji-color_1F916.svg";

  public Payload(String text) {
    this.text = text;
  }

  public Payload(String text, String channel) {
    this(text);
    this.channel = channel;
  }

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  @JsonProperty("username")
  public String getUserName() {
    return userName;
  }

  public void setUserName(String username) {
    this.userName = username;
  }

  @JsonProperty("icon_url")
  public String getIconUrl() {
    return iconUrl;
  }

  public void setIconUrl(String iconUrl) {
    this.iconUrl = iconUrl;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
