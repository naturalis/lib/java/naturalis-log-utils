package nl.naturalis.log.im;

public class InstantMessengerException extends Exception {

  public InstantMessengerException(String message, Throwable cause) {
    super(message, cause);
  }

  public InstantMessengerException(String message) {
    super(message);
  }

  public InstantMessengerException(Throwable cause) {
    super(cause);
  }
}
