package nl.naturalis.log.im;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.apache.http.entity.ContentType.APPLICATION_FORM_URLENCODED;

/**
 * InstantMessenger is a simple class for sending instant messages to either Mattermost and/or Slack
 * channel(s). Instantiate the class with an incoming webhook and you can use it to send messages or
 * payloads to a channel or a specified user.
 */
public class InstantMessenger implements AutoCloseable {

  private final String webhook;
  private final HttpClient httpClient;
  private final ObjectMapper mapper;

  /**
   * Instantiates an <code>InstantMessenger</code> that posts messages to the provided endpoint.
   *
   * @param webhook : the URL of the incoming webhook
   */
  public InstantMessenger(String webhook) {
    this.webhook = webhook;
    this.httpClient = HttpClientBuilder.create().build();
    this.mapper = configure(new ObjectMapper());
  }

  /**
   * Send a text message to the default channel.
   *
   * @throws InstantMessengerException
   */
  public void sendMessage(String message) throws InstantMessengerException {
    sendMessage(new Payload(message));
  }

  /**
   * Send a text message to the specified channel. To send a direct message to a specific person,
   * make sure to add a @ to the user name. E.g.: client.sendMessageToChannel("Hi John",
   * "@john.doe")
   *
   * @throws InstantMessengerException
   */
  public void sendMessage(String message, String channel) throws InstantMessengerException {
    sendMessage(new Payload(message, channel));
  }

  public void sendMessage(Payload payload) throws InstantMessengerException {
    try {
      StringEntity entity = new StringEntity(getRequestBody(payload), APPLICATION_FORM_URLENCODED);
      HttpPost request = new HttpPost(webhook);
      request.setEntity(entity);
      HttpResponse response = httpClient.execute(request);
      if (response.getStatusLine().getStatusCode() >= 400) {
        throw new InstantMessengerException(
            "Failed to send message: " + response.getStatusLine().getReasonPhrase());
      }
    } catch (IOException e) {
      throw new InstantMessengerException("Failed to send message", e);
    }
  }

  // Visible for testing
  String getRequestBody(Payload payload) throws JsonProcessingException {
    String v = mapper.writeValueAsString(payload);
    NameValuePair kv = new BasicNameValuePair("payload", v);
    return URLEncodedUtils.format(List.of(kv), StandardCharsets.UTF_8);
  }

  private static ObjectMapper configure(ObjectMapper mapper) {
    mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    return mapper;
  }

  @Override
  public void close() {
    if (httpClient instanceof CloseableHttpClient) { // which it is
      try {
        ((CloseableHttpClient) httpClient).close();
      } catch (IOException e) {
        // Too bad
      }
    }
  }
}
