package nl.naturalis.log;

/**
 * Symbolic constants for the logging implementations currently supported.
 *
 * @author Ayco Holleman
 */
public enum LogImpl {

  /** Logback */
  LOGBACK,
  /** Log4j (version 2 and higher) */
  LOG4J;
}
