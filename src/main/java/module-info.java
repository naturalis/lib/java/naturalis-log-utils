/**
 * Java module containing classes related to logging and instant messaging.
 * 
 * @author Tom Gilissen
 * @author Ayco Holleman
 *
 */
module nl.naturalis.log {

  exports nl.naturalis.log;
  exports nl.naturalis.log.appenders;
  exports nl.naturalis.log.im;

  exports nl.naturalis.log.appenders.log4j to junit;
  exports nl.naturalis.log.appenders.logback to junit;

  requires nl.naturalis.common;
  requires com.fasterxml.jackson.annotation;
  requires com.fasterxml.jackson.core;
  requires com.fasterxml.jackson.databind;
  requires junit;
  requires ch.qos.logback.classic;
  requires ch.qos.logback.core;
  requires org.apache.httpcomponents.httpclient;
  requires org.apache.httpcomponents.httpcore;
  requires org.apache.logging.log4j;
  requires org.apache.logging.log4j.core;
  requires org.slf4j;
}
