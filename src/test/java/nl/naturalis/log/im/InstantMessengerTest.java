package nl.naturalis.log.im;

import org.junit.Ignore;
import org.junit.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import static org.junit.Assert.assertEquals;

public class InstantMessengerTest {

  @Test
  public void getRequestBody_01() throws JsonProcessingException {
    try (InstantMessenger im = new InstantMessenger("not relevant")) {
      Payload p = new Payload("Hello World");
      p.setIconUrl(null);
      assertEquals("01", "payload=%7B%22text%22%3A%22Hello+World%22%7D", im.getRequestBody(p));
    }
  }

  @Test
  @Ignore
  public void getRequestBody_02() throws InstantMessengerException {
    String webhook = System.getenv("INSTANT_MESSENGER_EBDPOINT");
    try (InstantMessenger im = new InstantMessenger(webhook)) {
      Payload p = new Payload("Hello World!\nDit wordt een moeilijke... met \t tabs & % tekens en chinees:分ss秒\nNou doei\n\nAyco");
      im.sendMessage(p);
    }
  }

}
