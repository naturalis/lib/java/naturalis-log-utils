package nl.naturalis.log.appenders.logback;

import java.io.ByteArrayOutputStream;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.log.appenders.InvalidLoggerException;
import nl.naturalis.log.appenders.LiveLogStream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static nl.naturalis.log.LogImpl.LOGBACK;

/*
 * NB These tests are dependent on the configuration in logback-test.xml in src/main/resources!
 */

public class LogbackLiveLogStreamTest {

  private static final String NO_OUTPUT = "";

  private static final Logger fooLogger = LoggerFactory.getLogger("com.foo");
  private static final Logger barLogger = LoggerFactory.getLogger("com.bar");

  @Test // that LiveLogStream only picks up messages within the try-with-resources block
  public void test01() throws Exception {
    fooLogger.info("I am too early");
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    LiveLogStream stream = LiveLogStream.builderFor(LOGBACK)
        .withLoggers("com.foo")
        .withOutputStream(out)
        .build();
    try (stream) {
      fooLogger.info("My name is Foo");
    }
    fooLogger.info("I am too late");
    assertTrue("01", out.toString().contains("My name is Foo"));
    assertFalse("02", out.toString().contains("I am too early"));
    assertFalse("03", out.toString().contains("I am too late"));
  }

  @Test // that LiveLogStream picks up messages from selected loggers
  public void test02() throws Exception {
    fooLogger.info("I am too early");
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    LiveLogStream stream = LiveLogStream.builderFor(LOGBACK)
        .withLoggers("com.foo")
        .withOutputStream(out)
        .build();
    try (stream) {
      barLogger.info("My name is Bar");
    }
    fooLogger.info("I am too late");
    assertEquals("01", NO_OUTPUT, out.toString());
  }

  @Test // won't work b/c com.foo and com.bar are non-additive loggers
  public void test03() throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    LiveLogStream stream = LiveLogStream.builderFor(LOGBACK)
        .withOutputStream(out)
        .build(); // no logger specified so root logger
    try (stream) {
      fooLogger.info("My name is Foo");
      barLogger.info("My name is Bar");
    }
    assertEquals("01", NO_OUTPUT, out.toString());
  }

  @Test // works b/c additivity is temporarily set to true
  public void test04() throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    LiveLogStream stream = LiveLogStream.builderFor(LOGBACK)
        .withOutputStream(out)
        .withAdditiveLoggers("com.foo", "com.bar")
        .build(); // no logger specified so root logger
    try (stream) {
      fooLogger.info("My name is Foo");
      barLogger.info("My name is Bar");
    }
    // but now additivity should be restored to original value (false)
    fooLogger.info("I am too late");
    assertTrue("01", out.toString().contains("My name is Foo"));
    assertTrue("02", out.toString().contains("My name is Bar"));
    assertFalse("03", out.toString().contains("I am too late"));
  }

  @Test // with multiple loggers
  public void test05() throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    LiveLogStream stream = LiveLogStream.builderFor(LOGBACK)
        .withOutputStream(out)
        .withLoggers("com.foo", "com.bar")
        .build();
    try (stream) {
      fooLogger.info("My name is Foo");
      barLogger.info("My name is Bar");
    }
    assertTrue("01", out.toString().contains("My name is Foo"));
    assertTrue("02", out.toString().contains("My name is Bar"));
  }

  @Test // won't work b/c configured level for com.foo and com.bar is INFO.
  public void test06() throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    LiveLogStream stream = LiveLogStream.builderFor(LOGBACK)
        .withOutputStream(out)
        .withLoggers("com.foo", "com.bar")
        .build();
    try (stream) {
      fooLogger.debug("My name is Foo");
      barLogger.debug("My name is Bar");
    }
    assertEquals("01", NO_OUTPUT, out.toString());
  }

  @Test // works b/c log level is temporarily set to DEBUG
  public void test07() throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    LiveLogStream stream = LiveLogStream.builderFor(LOGBACK)
        .withOutputStream(out)
        .withLoggers("com.foo", "com.bar")
        .withLogLevel("DEBUG")
        .build();
    try (stream) {
      fooLogger.debug("My name is Foo");
      barLogger.debug("My name is Bar");
    }
    // but now level should be reset to original value (INFO)
    fooLogger.debug("I am too late");
    assertTrue("01", out.toString().contains("My name is Foo"));
    assertTrue("02", out.toString().contains("My name is Bar"));
    assertFalse("03", out.toString().contains("I am too late"));
  }

  /*
   * Tests that our check on the logger works: we only work with existing loggers and don't allow the accidental creation of new ones.
   */
  @Test
  public void test08() throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    try {
      LiveLogStream.builderFor(LOGBACK)
          .withOutputStream(out)
          .withLoggers("i.do.not.exist")
          .build();
    } catch (IllegalArgumentException e) {
      assertEquals(InvalidLoggerException.noSuchLogger("i.do.not.exist").getMessage(), e.getMessage());
      return;
    }
    assertTrue(false);
  }

  /*
   * Tests that our check on the logger works: we only work with loggers with a known log level.
   */
  @Test
  public void test09() throws Exception {
    @SuppressWarnings("unused")
    Logger comLogger = LoggerFactory.getLogger("com");
    // "com" logger now exists but is not not defined in in logback-test.xml and therefore lacks a log level.
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    try {
      LiveLogStream.builderFor(LOGBACK)
          .withOutputStream(out)
          .withLoggers("com")
          .build();
    } catch (IllegalArgumentException e) {
      assertEquals(InvalidLoggerException.missingLogLevel("com").getMessage(), e.getMessage());
      return;
    }
    assertTrue(false);
  }
}
