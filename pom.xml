<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>nl.naturalis.log</groupId>
    <artifactId>naturalis-log-utils</artifactId>
    <version>2.0.4</version>

    <name>Naturalis Log Utils</name>


    <properties>

        <!-- ==================================================================== -->
        <!-- *** Build settings                                               *** -->
        <!-- ==================================================================== -->
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- ==================================================================== -->
        <!-- ***  Library dependencies                                        *** -->
        <!-- ***  Check for updates: mvn versions:display-dependency-updates  *** -->
        <!-- ***  Check dependencies: mvn dependency:analyze                  *** -->
        <!-- ==================================================================== -->
        <naturalis-common.version>3.0.8</naturalis-common.version>
        <apache-httpcore.version>4.4.15</apache-httpcore.version>
        <apache-httpclient.version>4.5.13</apache-httpclient.version>
        <jackson.version>2.13.3</jackson.version>
        <slf4j.version>2.0.12</slf4j.version>
        <logback.version>1.5.3</logback.version>
        <log4j.version>2.23.0</log4j.version>
        <junit.version>4.13.2</junit.version>

        <!-- ==================================================================== -->
        <!-- ***  Check for updates: mvn versions:display-plugin-updates      *** -->
        <!-- ***  Maven plugin versions                                       *** -->
        <!-- ==================================================================== -->
        <maven-enforcer-plugin.version>3.1.0</maven-enforcer-plugin.version>
        <maven-checkstyle-plugin.version>3.1.2</maven-checkstyle-plugin.version>
        <maven-compiler-plugin.version>3.10.1</maven-compiler-plugin.version>
        <maven-dependency-plugin.version>3.1.1</maven-dependency-plugin.version>
        <git-commit-id-plugin.version>4.0.0</git-commit-id-plugin.version>
        <maven-resources-plugin.version>3.3.0</maven-resources-plugin.version>
        <maven-surefire-plugin.version>3.0.0-M7</maven-surefire-plugin.version>

    </properties>

    <organization>
        <name>Naturalis Biodiversity Center</name>
        <url>http://www.naturalis.nl</url>
    </organization>


    <distributionManagement>
        <repository>
            <id>naturalis</id>
            <url>http://mvnrepository.naturalis.io/repository/naturalis/</url>
        </repository>
        <snapshotRepository>
            <id>naturalis</id>
            <url>http://mvnrepository.naturalis.io/repository/naturalis/</url>
        </snapshotRepository>
    </distributionManagement>

    <dependencies>
        <dependency>
            <groupId>nl.naturalis.common</groupId>
            <artifactId>naturalis-common</artifactId>
            <version>${naturalis-common.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <version>${log4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>${log4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpcore</artifactId>
            <version>${apache-httpcore.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>${apache-httpclient.version}</version>
        </dependency>
        <!-- TEST -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
        </dependency>
    </dependencies>

    <build>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>${maven-enforcer-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>${maven-checkstyle-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven-resources-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven-compiler-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>${maven-dependency-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>pl.project13.maven</groupId>
                    <artifactId>git-commit-id-plugin</artifactId>
                    <version>${git-commit-id-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven-surefire-plugin.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <id>enforce-versions</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>[3.8.6,)</version>
                                </requireMavenVersion>
                                <requireJavaVersion>
                                    <version>[17,)</version>
                                </requireJavaVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <configuration>
                    <dotGitDirectory>${project.basedir}/../.git</dotGitDirectory>
                    <generateGitPropertiesFile>true</generateGitPropertiesFile>
                    <generateGitPropertiesFilename>
                        ${project.build.outputDirectory}/${project.artifactId}.git.properties
                    </generateGitPropertiesFilename>
                    <format>properties</format>
                    <dateFormat>yyyy-MM-dd HH:mm:ss</dateFormat>
                </configuration>
                <executions>
                    <execution>
                        <id>generate-git-properties</id>
                        <phase>initialize</phase>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <executions>
                    <execution>
                        <id>check-style</id>
                        <configuration>
                            <configLocation>google_checks.xml</configLocation>
                            <encoding>UTF-8</encoding>
                            <failsOnError>true</failsOnError>
                            <consoleOutput>true</consoleOutput>
                            <includeTestSourceDirectory>true
                            </includeTestSourceDirectory>
                        </configuration>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <source>${maven.compiler.source}</source>
                    <target>${maven.compiler.target}</target>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>
        </plugins>
    </build>


</project>
